## Software pro přípravu a realizaci bezpečnostních her (VI20202022158-R1)

Software poskytuje prostředí umožňující kompletní přípravu, realizaci a vyhodnocení bezpečnostních her. Software umožňuje snížit náklady a zkrátit čas přípravy her pomocí inovativních postupů, jako je multitenantní uživatelský přístup, standardizace klíčových komponent a formátů, a optimalizace výpočetní infrastruktury s ohledem na snížení nároků na výpočetní zdroje a využití kontejnerové virtualizace. Software umožňuje jednoduchý přístup účastníků k hernímu rozhraní s minimálním zapojením lektorů. Lektorům poskytuje nástroje pro správu životního cyklu bezpečnostních her, konkrétně podporu přípravy, realizaci hry, sledování jejího průběhu a vyhodnocení výsledků.

### Obsah repozitářů

Tento repozitář slouží jako rozcestník pro služby a komponenty tohoto software.  Sekce *DevOps* zajišťuje nasazení celého prostředí. Další sekce zahrnují komponenty a služby pro backendové části (*Python Backend* a *Java Backend*) a frontendovou část (*Angular Frontend*) software.

Každá položka obsahuje podrobné instrukce k jejímu sestavení a spuštení.

### DevOps
- [KYPO CRP Helm](https://gitlab.ics.muni.cz/muni-kypo-crp/devops/kypo-crp-helm)
- [Ansible Roles](https://gitlab.ics.muni.cz/muni-kypo/ansible-roles)
 
### Python Backend
- [KYPO Sandbox Service](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-python/kypo-sandbox-service)

### Java Backend
- [KYPO Security Commons](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-security-commons)
- [KYPO Elasticsearch Service](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-elasticsearch-service)
- [KYPO Elasticsearch Documents](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-elasticsearch-documents)
- [KYPO User & Group](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-user-and-group)
- [KYPO Training](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-training)
 
### Angular Frontend
- [KYPO User & Group Agenda](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/agendas/kypo-user-and-group-agenda)
- [KYPO Training Agenda](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/agendas/kypo-training-agenda)
- [KYPO Sandbox Agenda](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/agendas/kypo-sandbox-agenda)

### Poděkování

<table>
  <tr>
    <td>![MV ČR](logo_mvcr.png "Logo MV ČR")</td>
    <td>
Tento software je výsledkem projektu Výzkum nových technologií pro zvýšení schopností odborníků na kyberbezpečnost – VI20202022158, který byl podpořen Ministerstvem vnitra ČR z Programu bezpečnostního výzkumu České republiky 2015-2022 (BV III/1-VS).
</td>
  </tr>
</table>
